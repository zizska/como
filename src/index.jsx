import React from "react";
import { render } from "react-dom";

import App from "./components/App";

// import all image assets in such a way file-loader/url-loader recognizes them
function importAll(r) {
  return r.keys().map(r);
}

// eslint-disable-next-line
const images = importAll(require.context("./assets/pictures/", false, /\.(png|jpe?g|svg)$/));

// leave service worker commented out for development
/* global document window navigator */

// if ("serviceWorker" in navigator) {
//   window.addEventListener("load", () => {
//     navigator.serviceWorker
//       .register("/sw.js")
//       .then(registration => {
//         console.log("SW registered: ", registration);
//       })
//       .catch(registrationError => {
//         console.log("SW registration failed: ", registrationError);
//       });
//   });
// }

render(<App />, document.getElementById("root"));
