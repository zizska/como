import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
  html {
    display: flex;
    flow-flow: row wrap;
    ${'' /* flex-wrap: row; */}
    align-items: center;
    justify-content: center;
  }
  body {
    @import url("https://fonts.googleapis.com/css?family=Roboto");
    font-family: "Roboto", regular;
    font-size: 14px;
    text-rendering: optimizeLegibility;
    white-space: nowrap;
  }
`;

export default GlobalStyles;
