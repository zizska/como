import React from "react";
import { em, rgba } from "polished";
import PropTypes from "prop-types";
import styled from "styled-components";

import icon from "../assets/icon-follow.svg";

const ButtonBorder = styled.button`
  background-color: ${rgba("#0080FF", 1)};
  border-radius: 1px;
  border: none;
  cursor: pointer;
  display: flex;
  float: right;
  ${'' /* justify-content: space-between; */}
  margin-top: 15px;
  margin-bottom: 15px;
  margin-left: auto;
  outline: none;
  padding: 0px 14px;
  :hover {
    background-color: ${rgba("#044FB3", 1)};
    border-radius: 1px;
  }
`;

const ButtonText = styled.p`
  color: white;
  font-face: Roboto-Regular;
  font-size: 14px;
  line-height: 1;
  padding-left: 10px;
`;

const ButtonIcon = styled.p`
  ${'' /* min-height: ${em("16px")}; */}
  min-height: 16px;
  min-width: 16px;
  ${'' /* min-width: ${em("16px")}; */}
  background-image: url(${icon});
`;

const Button = ({ text }) => (
  <ButtonBorder>
    <ButtonIcon />
    <ButtonText>{text}</ButtonText>
  </ButtonBorder>
);

Button.propTypes = {
  text: PropTypes.string,
};
Button.defaultProps = {
  text: "Follow",
};

export default Button;
