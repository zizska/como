import PropTypes from "prop-types";
import React from "react";
import styled from "styled-components";
import { rgba, em } from "polished";
import Button from "./Button";

// {
//   "profilePicture": "assets/pictures/marin.ivankovic.jpg",
//   "username": "marin.ivankovic",
//   "name": "Marin",
//   "biography": "Just a guy, who decided to go for it\n—\n📍 🇭🇷 living in Berlin 🇩🇪\n🚀 Startups | Fashion | Fitness\n👻 Snapchat: mrn_kovic",
//   "followers": 26561,
//   "following": 454,
//   "userId": 274836879,
//   "numPosts": 808,
//   "website": "",
//   "lastPost": "01/09/2018",
//   "email": "No email in bio"
// },

const StyledListItemWrapper = styled.div`
  align-items: center;
  border: 1px solid #f5f5f5;
  box-shadow: 0px 8px 21px ${rgba("#000000", 0.15)};
  display: flex;
  min-height: 90px;
  ${'' /* min-height: ${em("90px")}; */}
  min-width: 470px;
  ${'' /* min-width: ${em("470px")}; */}
  padding: 15px 25px;
`;

const ImageContainer = styled.img.attrs({
  src: props => `./${props.profilePicture}`,
  alt: props => props.alt,
})`
  border-radius: 50%;
  ${'' /* height: ${em("60px")}; */}
  height: 60px;
  ${'' /* width: ${em("60px")}; */}
  width: 60px;
  margin-top: 15px;
  margin-bottom: 15px;
  margin-right: 20px;
  position: relative;
`;

const Heading = styled.p`
  font-size: 16px;
  line-height: 1.6;
  margin: 10px;
`;

const Subheading = styled.p`
  color: grey;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 40%;
  line-height: 1.5;
`;
/*
add conditional rendering for displaying website if no biography
unclear what to do if both strings are empty
*/
const ListItem = ({ profilePicture, username, name, biography }) => (
  <StyledListItemWrapper>
    <ImageContainer src={profilePicture} alt={name} />
    <Heading>{`@${username}`}</Heading><br />
    <Subheading>{`${biography}`}</Subheading>
    <Button />
  </StyledListItemWrapper>
);

ListItem.propTypes = {
  profilePicture: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  biography: PropTypes.string.isRequired,
  // website: PropTypes.string.isRequired,
};

export default ListItem;
