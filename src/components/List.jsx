/* eslint-disable react/jsx-key */
import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { em, rgba } from "polished";

import ListItem from "./ListItem";

const ListWrapper = styled.div`
  margin: 40px;
  height: 500px;
  max-width: 470px;
  fill: ${rgba("#FFFFFF", 1)};
  ${'' /* height: ${em("500px")}; */}
  ${'' /* max-width: ${em("470px")}; */}
  ${'' /* box-shadow: 0px 8px 21px ${rgba("#000000", 0.15)}; */}
`;

const List = ({ followerSuggestions }) => (
  <ListWrapper>
    {followerSuggestions.map(suggestion => (
      <ListItem
        biography={suggestion.biography}
        name={suggestion.name}
        profilePicture={suggestion.profilePicture}
        username={suggestion.username}
        website={suggestion.website}
      />
    ))}
  </ListWrapper>
);

List.propTypes = {
  followerSuggestions: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default List;
