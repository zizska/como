import { hot } from "react-hot-loader";
import React from "react";

import GlobalStyles from "../theme";
import List from "./List";

const followerSuggestions = require("../resources/follower-suggestions.json");

const App = () => (
  <div>
    <GlobalStyles />
    <List followerSuggestions={followerSuggestions} />
  </div>
);

export default hot(module)(App);
