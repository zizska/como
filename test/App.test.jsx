/* global expect, describe, test, beforeEach */
import React from "react";
import { shallow } from "enzyme";
import App from "../src/components/App";
import styled from "styled-components";
import "jest-styled-components";

describe("App", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<App />);
  });
  test("App renders without crashing", () => {
    expect(wrapper.exists()).toEqual(true);
  });
  test("App matches snapshot", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
