/* global expect, describe, test */
import React from "react";
import { shallow } from "enzyme";
// import styled from "styled-components";
import "jest-styled-components";

import IconButton from "../src/components/Button";

describe("Follow Button Component", () => {
  test("renders correctly", () => {
    const wrapper = shallow(<IconButton />);
    expect(wrapper).toMatchSnapshot();
  });
});
