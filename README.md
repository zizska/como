Issues encountered:
* Not readily apparent what to display for certain user biographies
  * A small subset of users had either:
    * no biography but a website (Beyonce)
    * no biography and no website (Kanye)
  * No data source available in the JSON to make the user match the design spec.

* Incorrect spelling for one of the file imports.
  * Fixed by hand in this instance.
  * Profile picture filenames seem to be the user's screenname.
    * This does not apply to duplicate users.

* Many values in px, which will not necessarily scale well for mobile devices.
* Importing directories (and subdirectories) of images so that they are properly recognized by webpack can be slightly tedious.
* Writing styled components in such a way that they can easily be ported to React Native can be a little cumbersome (no classNames).
* In retrospect, I probably should have opted to use BEM to organize my styling and maximize reusability.
* Getting the title and the subtitle to stack properly gave me more trouble than it should have.
* This also applies to getting the box-shadow to properly apply to the list wrapper.

Had I had more time, I wanted to limit the number of users shown on screen to 5, and then fetch more when the user scrolled to a certain threshold on their screen. I would've leveraged the Intersection Observer API and some masking CSS for the placeholders for loading data.

In a production environment, I would probably want to use primarily server-side rendering to keep bundle sizes down

All told, the assignment took me about 10 hours.
