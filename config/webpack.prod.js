const merge = require("webpack-merge");
const path = require("path");
const common = require("./webpack.common");

const DIST_DIR = path.join(__dirname, "../dist");

module.exports = merge(common, {
  mode: "production",
  stats: {
    colors: false,
    hash: true,
    timings: true,
    assets: true,
    chunks: true,
    chunkModules: true,
    modules: true,
    children: true,
    warnings: true,
  },
  optimization: {
    minimize: true,
    sideEffects: true,
  },
  output: {
    path: DIST_DIR,
    filename: "[name].bundle.[chunkhash].js",
  },
  module: {
    rules: [
      {
        test: /\.(gif|jpe?g|png|svg)$/i,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 8192,
            },
          },
          {
            loader: "img-loader",
            options: {
              plugins: [
                require("imagemin-mozjpeg")({
                  progressive: true,
                  arithmetic: false,
                }),
                require("imagemin-svgo")({
                  plugins: [{ removeTitle: true }, { convertPathData: false }],
                }),
              ],
            },
          },
        ],
      },
      {
        test: /\.(ttf|eot|woff|woff2)$/i,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 50000,
            },
          },
        ],
      },
    ],
  },
});
