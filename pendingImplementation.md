notes on implementation:
dynamic importing of image directories remains tedious


considerations:
how will the list grow? add lazy-loading or have a fetch method of some kind trigger when user scrolls down sufficient amount


convert sizes to percentages to scale appropriately for mobile devices

not yet implemented:
lazy-loading

expandable subheading text on click
not yet properly functional

line break between text components in list item

special workflow for testing styled-components with jest; most of the examples use react-test-renderer rather than enzyme
